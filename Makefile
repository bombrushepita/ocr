CC = gcc

CFLAGS = `sdl2-config --cflags` `pkg-config --cflags gtk+-3.0` -Wunused-parameter -fsanitize=address -Wextra -Wno-unused-parameter -Wall -Werror -g 
LDFLAGS = `pkg-config --libs SDL2_image` `pkg-config --libs gtk+-3.0` -lm

SRC = code/image_processing/image_processing.c\
code/image_processing/basic_functions.c\
code/image_processing/rotate.c\
code/image_processing/splitImage.c\
code/image_processing/thresholding.c\
code/image_processing/gridDetection.c\
code/solver/solver.c\
code/solver/solve.c\
code/solver/verif.c\
code/solver/return_image.c\
code/solver/return_image.c\
code/neural_network/neural_network.c\
code/neural_network/neuron.c\
code/neural_network/save_weights.c\
code/neural_network/basic_functions.c\
code/neural_network/training.c\
code/neural_network/find_result.c\
code/graphical_interface/menu.c\
code/graphical_interface/basic_functions.c
OBJ = ${SRC:.c=.o}

all: image_processing neural_network solver sudoku_solver

image_processing: code/image_processing/image_processing.o\
code/image_processing/basic_functions.o\
code/image_processing/rotate.o\
code/image_processing/splitImage.o\
code/image_processing/thresholding.o\
code/image_processing/gridDetection.o
	${CC} ${CFLAGS} $^ -o ./bin/$@ ${LDFLAGS}


neural_network: code/neural_network/neural_network.o\
code/neural_network/neuron.o\
code/neural_network/save_weights.o\
code/neural_network/basic_functions.o\
code/neural_network/training.o\
code/neural_network/find_result.o
	${CC} ${CFLAGS} $^ -o ./bin/$@ ${LDFLAGS}


solver: code/solver/solver.o\
code/solver/solve.o\
code/solver/verif.o\
code/solver/return_image.o
	${CC} ${CFLAGS} $^ -o ./bin/$@ ${LDFLAGS}


sudoku_solver:code/image_processing/rotate.o\
code/image_processing/splitImage.o\
code/image_processing/thresholding.o\
code/image_processing/gridDetection.o\
code/neural_network/neuron.o\
code/neural_network/save_weights.o\
code/neural_network/training.o\
code/neural_network/find_result.o\
code/solver/verif.o\
code/solver/solve.o\
code/solver/return_image.o\
code/graphical_interface/menu.o\
code/graphical_interface/basic_functions.o

	${CC} ${CFLAGS} $^ -o ./bin/$@ ${LDFLAGS}


clean:
	${RM} ${OBJ}
	${RM} ./bin/*
	${RM} -rf ./output/*
	${RM} -rf ./src/*.result


.PHONY: clean all