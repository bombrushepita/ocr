# OCR Sudoku solver

<!--link to the gitlab : https://gitlab.com/bombrushepita/ocr -->

![Image text](needed/result.png)

## Table of Contents
1. [General Info](#general-info)
2. [The Authors](#the-authors)
3. [Libraries](#libraries)
4. [Installation](#installation)
5. [Using the app](#using-the-app)
6. [FAQs](#faqs)


### General info
This project is an Optical Character Recognition (OCR) Sudoku solver, It can solve a sudoku grid from a picture and returns you a new picture containing the completed grid. It is completely coded in C, and uses some libraries

### The Authors
![Image text](needed/logo_daftlion_cropped.jpg)

This is a project from the Daft Lion team. If you want to learn more about us, you can check our [web site](https://daftlion.wixsite.com/studio) or our [Instagram](https://www.instagram.com/daft.lion/)


## Libraries
***
A list of the libraries used within the project:
* [GTK](https://www.libsdl.org/download-2.0.php): Version 2.0.18
* [SDL2](https://www.gtk.org/): Version 4.4.1
You need to download those libraries in order to use the OCR Sudoku solver


## Installation
***
To install and start the OCR Sudoku slover, you have to run the code below. Don't forget to download the libraries used by the application before running it or it will not work.
```
$ git clone https://gitlab.com/bombrushepita/ocr.git
$ cd ocr/
$ make
$ ./bin/sudoku_solver
```

## Using the app

![Image text](needed/interfaceResize.png)

**Starting the app** (if it is not already done)
* You need to be at the root of the application file (path_to_the_file/ocr/) then run those command lines
```
$ make
$ ./bin/sudoku_solver
```

In order to solve your Sudoku, we have to prepare it. Follow the steps using the menu on the right. Every step is unlocked only if the previous step is already completed.

For the Binarization, you have to choose 2 values. The first one is the Treshold value, and the second one is the Blurring value (it is used before the Thresholding).

_If your image is clear and in a good quality, we suggest you to keep 20 and 20 as the Binarization values._

You can always use the "<= Back" button to return to the previous step.

_You normally don't need to use the IA training button as it could cause the IA to be less efficient._




## FAQs
***
A list of frequently asked questions
1. **Why would I use this app ?**
* We suggest you to use this app if you can't solve a Sudoku. It can help you understand where and why you are stuck. Note the it can only entirely complete the Sudoku, and not partially.
2. __How does it work ?__
* First : The image is binarized
* Second : We split the different boxes
* Third : The Artificial Intelligence understands which numbers are in the boxes
* Fourth : The sudoku is sloved
* Fifth : We create an image of the completed grid
* _note that you can find a more precise description of the project on the [web site](https://daftlion.wixsite.com/studio) (in french only)_

3. **What will happend if I chose an image that does not contain a Sudoku ?**

* The application will not ba able to find a grid when trying to resize the image, and it will crash instantly. _Sad_

