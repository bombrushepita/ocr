#ifndef VERIF_H
#define VERIF_H

int is_column_solved(int grid[81], int x);
int is_line_solved(int grid[81], int y);
int is_square_solved(int grid[81] , int x, int y);
int is_solved(int grid[81]);

#endif
