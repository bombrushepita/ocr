#ifndef SOLVE_H
#define SOLVE_H

int already_in_column(int grid[81], int x, int val);
int already_in_line(int grid[81], int y, int val);
int already_in_square(int grid[81], int x, int y, int val);
int solve_rec(int grid[81],int x, int y);
int solve(int grid[81]);
void printGrid(int grid[81]);

#endif

