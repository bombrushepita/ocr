#include "solve.h"
#include <err.h>
#include "verif.h"
#include <stdio.h>


/*******************************************************************************/
/*                    Checking for the presence of a number                    */
/*******************************************************************************/


int already_in_column(int grid[81], int x, int val)
{
  int found = 0;
  for (int i = 0; i < 9 && !found; i++)
  {
    if (grid[i * 9 + x] == val)
      found = 1;
  }
  return found;
}


int already_in_line(int grid[81], int y, int val)
{
  int found = 0;
  for (int i = 0; i < 9 && !found; i++)
  {
    if (grid[y * 9 + i] == val)
      found = 1;
  }
  return found;
}



int already_in_square(int grid[81], int x, int y, int val)
{
  int found = 0;
  int xDebut = x - x % 3;
  int yDebut = y - y % 3;
  for (int i = xDebut; i < 3 + xDebut && !found; i++)
  {
    for (int j = yDebut; j < 3 + yDebut && !found; j++)
    {
      if (grid[j * 9 + i] == val)
        found = 1;
    }
  }
  return found;
}



/*******************************************************************************/
/*                             Solving the sudoku                              */
/*******************************************************************************/



int solve_rec(int grid[81],int x, int y)
{ 
  int val = grid[x * 9 + y]; // val corresponds to the value which was there before
  int pose = 0;
  for (int i = val + 1; i < 10 && !pose; i++)
  {
    if(!already_in_column(grid, y, i) && !already_in_line(grid, x, i) && !already_in_square(grid, y, x, i))
    {
      // if the conditions are verified, we return true and we write the value in the grid
      pose = 1;
      grid[x * 9 + y] = i;
    }
  }
  // else we return false and we write 0 in the grid
  if(!pose)
    grid[x * 9 + y] = 0;
  return pose;
}






int solve(int grid[81])
{
  //Creation of a table which help to know where was the numbers present before solving
  int isEmpty[9][9];
  for (int i = 0; i < 9; i++)
  {
    for (int j = 0; j < 9; j++)
    {
      if (grid[i * 9 + j] == 0)
        isEmpty[i][j] = 1;
      else
        isEmpty[i][j] = 0;
    }
  }

  int x = 0;
  int y = 0;
  int back = 0;

  //Sudoku solving loop
  
  while (!is_solved(grid))
  {
    // Allow online movement on the sudoku and go up or down if necessary.
    if (x > 8)
    {
      y++;
      x = 0;
    }
    if (x < 0)
    {
       y--;
       x = 8;
    }

    // Check if the sudoku is possible
    if (y == -1 || y == 9)
    {
      return 1;
    }

    // If the box had is empty then we do the test of solve_rec

    if (isEmpty[x][y])
    {
      if (solve_rec(grid, x, y))
      {
        back = 0;
        x++;
      }
      else
      {
        back = 1;
        x--;
      }
    }
    // Else, we pass over
    else
    {
      if (back)
        x--;
      else
        x++;
    }
  }
  return 0;
}


/*******************************************************************************/
/*                             Printing the sudoku                             */
/*******************************************************************************/


void printGrid(int grid[81])
{
  for(int i = 0; i < 9; i++)
  {
    for(int j = 0; j < 9; j ++)
    {
      if(grid[i * 9 + j] == 0)
        printf(".");
      else
        printf("%i",grid[i * 9 + j]);
    }
    printf("\n");
  }
  printf("\n\n\n\n");
}
