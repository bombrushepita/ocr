#include "verif.h"


/*******************************************************************************/
/*           Checking if the differents parts of sudoku are solved             */
/*******************************************************************************/


int is_column_solved(int grid[81] , int x)
{
  int solved = 1;
  for (int i = 0; i < 9 && solved; i++)
  {
    if (grid[i * 9 + x] == 0)
    solved = 0;
    else
    {
      for (int j = i + 1; j < 9; j++)
      {
        if (grid[i * 9 + x] == grid[j * 9 + x])
            solved = 0;
      }
    }
  }
  return solved;
}






int is_line_solved(int grid[81] , int y)
{
  int solved = 1;
  for (int i = 0; i < 9 && solved; i++)
  {
    if (grid[y * 9 + i] == 0)
      solved = 0;
    else
    {
      for (int j = i + 1; j < 9; j++)
      {
        if (grid[y * 9 + i] == grid[y * 9 + j])
          solved = 0;
      }
    }
  }
  return solved;
}






int is_square_solved(int grid[81], int x, int y)
{
  int solved = 1;
  int xDebut = x - x % 3;
  int yDebut = y - y % 3;
  for (int i = xDebut; i < 3 + xDebut && solved; i++)
  {
    for (int j = yDebut; j < 3 + yDebut && solved; j++)
    {
      if (grid[j * 9 + i] == 0)
        solved = 0;
      else
      {
        for (int iBis = i ; iBis < 3; iBis++)
        {
          for (int jBis = j; jBis < 3; jBis++)
          {
            if (iBis == i)
              jBis += 1;
            if (grid[iBis * 9 + jBis] == grid[i * 9 + j])
              solved = 0;
          }
        }
      }
    }
  }          
  return solved;
}






int is_solved(int grid[81])
{
  int line_solved = 1;
  for (int x = 0; x < 9 && line_solved; x++)
  {
    line_solved = is_line_solved(grid, x);
  }
  if (!line_solved)
    return 0;

  int column_solved = 1;
  for (int y = 0; y < 9 && column_solved; y++)
  {
    column_solved = is_column_solved(grid, y);
  }
  if (!column_solved)
    return 0;

  int square_solved = 1;
  for (int x = 0; x < 3 && square_solved; x++)
  {
    for (int y = 0; y < 3 && square_solved; y++)
    {
      square_solved = is_square_solved(grid, x * 3, y * 3);
    }
  }
  return square_solved;
}

