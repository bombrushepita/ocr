#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include "solve.h"
#include "return_image.h"

int main(int argc, char** argv)
{
  //Verification of the number of arguments
  if(argc != 2)
    errx(1,"Error number of arguments");  

  //Opening of the file
  FILE* f;
  f = fopen(argv[1],"r");
  if(f==NULL)
      errx(1, "Error during opening of the file");
  
  //Loading the data from the file into the sudoku table (sdk)
  char str[15] = "";
  int sdk[81];

  int line = 0;
  while (fgets(str,14,f) != NULL)
  { 
    if(str[0] != '\n')
    {
      int j = 0;
      int column = 0;
      while(column < 9)
      {
        if(str[j] == '.')
        {
          sdk[line * 9 + column] = 0;
          column++;
        }
        if(str[j] >= '1' && str[j] <= '9')
        {
          sdk[line * 9 + column] = str[j] - '0';
          column++;
        }
        j++;
      }
      line++;
    }
  }

  //Closing the file
  fclose(f);
  
  //Call of the function to solve the sudoku 
  solve(sdk);
  
  //Finding the name of the file of return 
  char *fileName = argv[1];
  strcat(fileName,".result");

  //Opening of the file of return and writing on it 
  FILE* f2;
  f2 = fopen(fileName,"w");

  for(int i = 0; i< 9; i++)
  {
    if(i % 3 == 0 && i != 0)
      fputc('\n',f2);
    for(int j = 0;j < 9; j++)
    {
      if(j % 3 == 0 && j != 0)
        fputc(' ',f2);
      fputc(sdk[i * 9 + j] + '0', f2);
    }
    fputc('\n',f2);
  }

  //Closing the file and return of the main
  fclose(f2);

  //Create image
  return_image(fileName);

  return 0;
}

