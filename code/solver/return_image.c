#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "return_image.h"
#include "../image_processing/basic_functions.h"



void load_sdk(char* filename, int sdk[9][9]);


void return_image(char *filename)
{
	int sdk[9][9];
	load_sdk(filename,sdk);

	SDL_Surface* empty_sudoku = NULL;

	char sdkFile[25];

    snprintf(sdkFile, 25, "needed/empty_sudoku.jpg");

    empty_sudoku = IMG_Load(sdkFile);

    if (empty_sudoku == NULL)
    {
        SDL_Log("ERROR : Loading image > %s\n",IMG_GetError());
        exit(EXIT_FAILURE);
    }

	SDL_Surface* numbers[9];

	for(int i = 0; i < 9; i++)
    {
        char numberFile[15];

        snprintf(numberFile, 15, "needed/%d.png", i + 1);

        SDL_Surface* picture = IMG_Load(numberFile);

        if (picture == NULL)
        {
            SDL_Log("ERROR : Loading image > %s\n",IMG_GetError());
            exit(EXIT_FAILURE);
        }

        numbers[i] = picture;

    }



    for(int i = 0; i < 9; i++)
    {
    	for(int j = 0; j < 9; j++)
    	{
   	    	SDL_Surface *surface = numbers[sdk[j][i] - 1];
    		SDL_Rect rectDST = {i * 60 + 10, j * 60 + 10, 200, 200};
    		SDL_Rect rectSRC = {5, 5, 120, 120};

    		if (SDL_BlitSurface(surface, &rectSRC, empty_sudoku, &rectDST) != 0)
        	{
            	SDL_Log("ERROR : Creating cut image > %s\n",IMG_GetError());
            	exit(EXIT_FAILURE);
        	}
    	}
    }

    char outputFile[18];

    snprintf(outputFile, 18, "output/result.png");
    IMG_SavePNG(empty_sudoku, outputFile);

    SDL_FreeSurface(empty_sudoku);

    for(int i = 0; i < 9; i++)
    {
    	SDL_FreeSurface(numbers[i]);
    }

}




void load_sdk(char* filename, int sdk[9][9])
{
	FILE* f;
  	f = fopen(filename,"r");
  	if(f==NULL)
      	errx(1, "Error during opening of the file");

  	//Loading the data from the file into the sudoku table (sdk)
  	char str[15] = "";

  	int line = 0;
  	while (fgets(str,14,f) != NULL)
  	{
    	if(str[0] != '\n')
    	{
      		int j = 0;
      		int column = 0;
      		while(column < 9)
      		{
        		if(str[j] == '.')
        		{
          			sdk[line][column] = 0;
          			column++;
        		}
        		if(str[j] >= '1' && str[j] <= '9')
        		{
          			sdk[line][column] = str[j] - '0';
          			column++;
        		}
        		j++;
      		}
      		line++;
    	}
  	}

  	//Closing the file
  	fclose(f);
}
