#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>

#include <gtk/gtk.h>

#include "neuron.h"
#include "save_weights.h"

/****************************************************************************/
/*                        Save weights and biaises                          */
/****************************************************************************/

void Neuron_Save(Neuron *this, FILE *file, int prevSize)
{
  for(int i = 0; i < prevSize; i++)
  {
    fprintf(file," %lf",this->weights[i]);
  }
  fprintf(file,"/%lf",this->biais);
  return;
}
 
void Layer_Save(Layer *this, int len, FILE *file)
{
  for(int i = 0; i < len; i++)
  {
    Neuron_Save(&(this->neurons[i]),file, this->prevSize);
  }
  return;
}


void SaveNetworkData(Network *this, int size_layer[], char *path)
{
  FILE *file;
  file = fopen(path,"w");

  for(int i = 0; i < this->size; i++)
  {
    Layer_Save(&(this->layers[i]), size_layer[i], file);
    fprintf(file,"\n");
  }
  fclose(file);
}



/*****************************************************************************/
/*                    Recovery of weights and biaises                        */
/*****************************************************************************/


void mystrcat(char *str, char letter)
{
    int i = 0;
    while(str[i] != '\0')
      i++;
    str[i] = letter;
    str[i + 1] = '\0';
}


Network Recovery(char *path, int size_layer[])
{
  FILE *file;
  file = fopen(path,"r");

  if(file == NULL)
    errx(1, "The file doesn't exist");
  
  Network net = Network_Create(size_layer, 3);
  
  int nbLayer = 0;  
  int nbNeuron = 0;
  int i = 0;

  char letter = fgetc(file);

  while(letter != EOF)
  {
      
      if(letter == '\n')
      {
          nbLayer += 1;
          nbNeuron = 0;
          i = 0;
          letter = fgetc(file);
      }


      else if(letter == '/')
      {
        char *str = (char *) calloc(15,sizeof(char));

        letter = fgetc(file);
        while(letter != ' ' && letter != '\n' && letter != '/' && letter != EOF)
        {
            mystrcat(str,letter);
            letter = fgetc(file);
        }
        
        net.layers[nbLayer].neurons[nbNeuron].biais = g_strtod(str,NULL);
        nbNeuron += 1;
        i = 0;

        free(str);
      }



      else if(letter == ' ')
      {

        char *str = (char *) calloc(15,sizeof(char));

        letter = fgetc(file);
        while(letter != ' ' && letter != '\n' && letter != '/' && letter != EOF)
        {
            mystrcat(str,letter);
            letter = fgetc(file);
        }

        net.layers[nbLayer].neurons[nbNeuron].weights[i] = g_strtod(str,NULL);
        i += 1;

        free(str);  
      }
  }

  fclose(file);
  return net;
}



