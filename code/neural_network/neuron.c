#include "neuron.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

static void Neuron_Init(Neuron *this, int prevSize);
static void Layer_Init(Layer *this, int size, int prevSize);
static void Network_Init(Network *this, int size_layers[], int len);

double randoms();

/*******************************************************************************/
/*      Creation and Initialisation of the elements of the neural network      */
/*******************************************************************************/


Neuron Neuron_Create(int prevSize)
{
  Neuron this;
  Neuron_Init(&this,prevSize);
  return this;
}



Layer Layer_Create(int size, int prevSize)
{
  Layer this;
  Layer_Init(&this,size,prevSize);
  return this;
}



Network Network_Create(int size_layers[], int len)
{
  Network this;
  Network_Init(&this,size_layers,len);
  return this;
}



static void Neuron_Init(Neuron *this, int prevSize)
{
    this->prevSize = prevSize;
    this->value = 0;
    this->biais = randoms();
    if(prevSize > 0)
    {
      double *tab = (double *) calloc(prevSize, sizeof(double));
      for(int i = 0; i < prevSize; i++)
      {
        tab[i] = randoms();
      }
      this->weights = tab;
    }
    else
    {
      this->weights = NULL;
    }
    return;
}



static void Layer_Init(Layer *this, int size, int prevSize)
{
    this->size = size;
    this->prevSize = prevSize;
    Neuron *tab = (Neuron *) calloc(size, sizeof(Neuron));
    for(int i = 0; i < size; i++)
    {
      tab[i] = Neuron_Create(prevSize);
    }
    this->neurons = tab;
    return;
}



static void Network_Init(Network *this, int size_layers[], int len)
{
    this->size = len;
    Layer *tab = (Layer *) calloc(len, sizeof(Layer));
    tab[0] = Layer_Create(size_layers[0], 0);
    for(int i = 1; i < len; i++)
    {
      tab[i] = Layer_Create(size_layers[i], size_layers[i-1]);
    }
    this->layers = tab;
    return;
}





/****************************************************************************/
/*          Printing of elements of the neural network for test             */
/****************************************************************************/


void Neuron_Print(Neuron this)
{
  for(int i = 0; i < this.prevSize; i++)
    printf("%lf,", this.weights[i]);
  printf(" biais = %lf,  value = %f",this.biais, this.value);
  return;
}


void Layer_Print(Layer this, int len)
{
  for(int i = 0; i < len; i++)
  {
    Neuron_Print(this.neurons[i]);
    printf("\n");
  }
  return;
}


void Network_Print(Network this, int len, int size_layer[])
{
  for(int i = 0; i < len; i++)  
  {
    Layer_Print(this.layers[i],size_layer[i]);
    printf("\n\n\n");
  }
  return;
}



void printImage(double tab[])
{
    for(int i = 0; i < 28; i++)
    {
       for(int j = 0; j < 28; j++)
        {
            printf("%d",(int)tab[i * 28 + j]);
        } 
        printf("\n");
    }
    printf("\n\n\n\n\n");   
}



/****************************************************************************/
/*                          Free memory neurons                             */
/****************************************************************************/

void Neuron_Free(Neuron *this)
{
  free(this->weights);
  return;
}


void Layer_Free(Layer *this)
{
  for(int i = 0; i < this->size; i++)
  {
    Neuron_Free(&(this->neurons[i]));
  }
  free(this->neurons);
  return;
}


void Network_Free(Network *this)
{
  for(int i = 0; i < this->size; i++)
  {
    Layer_Free(&(this->layers[i]));
  }
  free(this->layers);
  return;
}



/****************************************************************************/
/*                           Function Sigmoid                               */
/****************************************************************************/

double sigmoid(double z)
{
  double res = exp(-z);
  return 1 / (1 + res);
}



double sigmoid_prime(double z)
{
  return z*(1-z);
}




/******************************************************************************/
/*                                 Propagation                                */
/******************************************************************************/


double sumInput(Neuron *this, Layer *prevLayer)
{
  double sum = this->biais;
  for(int i = 0; i < this->prevSize; i++)
  {
    sum += prevLayer->neurons[i].value * this->weights[i];
  }
  return sum; 
}




void propagationLayer(Layer *layer, Layer *prevLayer)
{
  for(int i = 0; i < layer->size; i++)
  {
    layer->neurons[i].value = sigmoid(sumInput(&(layer->neurons[i]),prevLayer));
  }
  return;
}


void feedForward(Network *this, double inputs[], int nb_inputs, double res[])
{
    for(int i = 0; i < nb_inputs; i++)
    {
        this->layers[0].neurons[i].value = inputs[i];
    }

    for(int i = 1; i < this->size; i++)
    {
        propagationLayer(&(this->layers[i]),&(this->layers[i-1]));
    }  

    for(int i = 0; i < 9; i++)
    {
        res[i] = this->layers[2].neurons[i].value;
    }
}



/*******************************************************************************/
/*                               BackPropagation                               */
/*******************************************************************************/


double output_distance(double output_need, double result)
{
  return output_need - result;
}


void backpropagation(Network *this, double output[], double output_need[])
{

    double delta[9]; 
   
    for(int i = 0; i < 9; i++)
    {
        delta[i] = output_distance(output_need[i], output[i]) * sigmoid_prime(output[i]); 
    } 
  
    double *deltaH = calloc(this->layers[1].size,sizeof(double));


    for(int j = 0; j < this->layers[1].size; j++)
    {
        double sum = 0;
        for(int i = 0; i < this->layers[2].size; i++)
        {
            sum += delta[i] * this->layers[2].neurons[i].weights[j];

            this->layers[2].neurons[i].weights[j] += 0.01 * delta[i] * this->layers[1].neurons[j].value;
            this->layers[2].neurons[i].biais += 0.01 * delta[i];
        }
 
        deltaH[j] = sum * sigmoid_prime(this->layers[1].neurons[j].value);
    }

  
    for(int i = 0; i < this->layers[1].size; i++)
    {
        for(int j = 0; j < this->layers[0].size; j++)
        {
            this->layers[1].neurons[i].weights[j] += 0.01 * deltaH[i] * this->layers[0].neurons[j].value;
            this->layers[1].neurons[i].biais += 0.01 * deltaH[i];
        }
    }

    free(deltaH);
    return;
}

/**************************************************************************/
/*                            Random Number                               */
/**************************************************************************/





double randoms()
{
  static int first = 0;
  
  if (first == 0)
  {
    srand (time (NULL));
    first = 1;
  }
  double res = (double) rand();
  int sign = rand() % 2;
  if (sign)
    res = - res;
  return (double) (res / RAND_MAX);
}



double randoms_bis(int max)
{
  static int first = 0;
  
  if (first == 0)
  {
    srand (time (NULL));
    first = 1;
  }
  int res = rand();
  return res % max;
}





