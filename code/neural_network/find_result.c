#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "basic_functions.h"
#include "neuron.h"
#include "find_result.h"



// Function which complete the board "sdk" with the result find for each input
// If the input is empty, the board is complete by '.' in the main function

void return_result(double input[][784], Network* net, int sdk[])
{
    for(int i = 0; i < 9; i++)
    {
        for(int j = 0; j < 9; j++)
        {
            if(sdk[i * 9 + j] == 0)
            {
                double res[] = {0,0,0,0,0,0,0,0,0};
                feedForward(net,input[i * 9 + j],784, res);
            
                int result = max_tab(res);
                sdk[i * 9 + j] = result+1;
            }
        }
    }
}



// Function which save the result of the board "sdk" in a file
// It will able the solve the grid after by the solver exe

void save_result(int sdk[], char *fileName)
{
    FILE* f;
    f = fopen(fileName,"w");

    for(int i = 0; i< 9; i++)
    {
        if(i % 3 == 0 && i != 0)
            fputc('\n',f);
        for(int j = 0;j < 9; j++)
        {
            if(j % 3 == 0 && j != 0)
                fputc(' ',f);
            fputc(sdk[i * 9 + j] + '0', f);
        }
        fputc('\n',f);
    }

    //Closing the file and return of the main
    fclose(f);
}
