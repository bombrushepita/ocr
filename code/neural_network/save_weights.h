#ifndef SAVE_WEIGHTS_H
#define SAVE_WEIGHTS_H

#include "neuron.h"
#include <stdio.h>

void Neuron_Save(Neuron *this, FILE *file, int prevSize);
void Layer_Save(Layer *this, int len, FILE *file);
void SaveNetworkData(Network *this, int size_layer[], char *path);

Network Recovery(char *path, int size_layer[]);

#endif