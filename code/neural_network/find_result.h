#ifndef FIND_RESULT_H
#define FIND_RESULT_H

void return_result(double input[][784], Network* net, int sdk[]);
void save_result(int sdk[], char *fileName);

#endif