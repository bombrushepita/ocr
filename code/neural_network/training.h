#ifndef TRAINING_H
#define TRAINING_H

int training(double *input[], double output[][9], int nb_image, Network* net);
void testing(double input[][784], int len, Network* net);

#endif