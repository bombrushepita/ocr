#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <err.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "basic_functions.h"
#include "neuron.h"
#include "save_weights.h"
#include "training.h"
#include "find_result.h"



void print_result(int sdk[], char *fileName)
{
    FILE* f;
    f = fopen(fileName,"w");

    for(int i = 0; i< 9; i++)
    {
        if(i % 3 == 0 && i != 0)
            fputc('\n',f);
        for(int j = 0;j < 9; j++)
        {
            if(j % 3 == 0 && j != 0)
                fputc(' ',f);
            fputc(sdk[j * 9 + i] + '0', f);
        }
        fputc('\n',f);
    }

    //Closing the file and return of the main
    fclose(f);
}



int main(int argc, char *argv[])
{
    int network_size[] = {784,30,9}; 
  
    char c = 0;

    while(c != 'n' && c != 'N' && c != 'y' && c != 'Y')
    {
        printf("Do you want to recover your neural network ? y/n\n");
        scanf("%c",&c);

        while(c == '\n')
            scanf("%c",&c);
    }

    Network net;

    if(c == 'n' || c == 'N')
        net = Network_Create(network_size,3);

    else if(c == 'y' || c == 'Y')
        net = Recovery("weights_biais",network_size);



    char val = 0;

    while(val != 'n' && val != 'N' && val != 'y' && val != 'Y')
    {
        printf("Do you want to train your neural network ? y/n\n");
        scanf("%c",&val);

        while(val == '\n')
            scanf("%c",&val);
    }


    if(val == 'y' || val == 'Y')
    {
        if(argc != 2)
            errx(1,"Just needed number of itération for training");

        IMG_Init(IMG_INIT_JPG);

        int nb_image = 515;

        int nb_iterations = atoi(argv[1]);

        for(int g = 0; g < nb_iterations; g++)
        {
            double *input[9];

            for(int i = 0; i < 9; i++)
                input[i] = calloc(784,sizeof(double));

            double output[9][9];

            SDL_Surface *picture = NULL;

            int i = randoms_bis(nb_image);

            for(int j = 0; j < 9; j++)
            {
                char outputFile[35];

                snprintf(outputFile, 35, "training_data/%d/image%d.png", j + 1 , i + 1);

                picture = IMG_Load(outputFile);

                if(picture == NULL)
                {
                    SDL_Log("ERROR : Loading image > %s\n",IMG_GetError());
                    exit(EXIT_FAILURE);
                }


                for(int k = 0; k < 9; k++)
                    output[j][k] = 0;
                output[j][j] = 1;

                image_to_mat(picture,input[j]);

                SDL_FreeSurface(picture);
            }

            training(input, output, 9, &net);

            for(int k = 0; k < nb_image * 9; k++)
            {
                free(input[k]);
            }

        }



        c = 0;

        while(c != 'n' && c != 'N' && c != 'y' && c != 'Y')
        {
            printf("Do you want to save your neural network ? y/n\n");
            scanf("%c",&c);

            while(c == '\n')
                scanf("%c",&c);

            printf("\n\n");
        }

        if(c == 'y' || c == 'Y')
            SaveNetworkData(&net,network_size,"weights_biais");

    }


    else
    {

        c = 0;

        while(c != 'n' && c != 'N' && c != 'y' && c != 'Y')
        {
            printf("Do you want to find result for output ? y/n\n");
            scanf("%c",&c);

            while(c == '\n')
                scanf("%c",&c);
        }

        if(c == 'y' || c == 'Y')
        {
            double input[81][784];

            int sdk[81];

            for(int i = 0; i < 81; i++)
                sdk[i] = 0;

            SDL_Surface *picture = NULL;

            for(int i = 0; i < 9; i++)
            {
                for(int j = 0; j < 9; j++)
                {
                    char outputFile[35];
                
                    snprintf(outputFile, 35, "output/test%d%d.png", i, j);
                
                    picture = IMG_Load(outputFile);
        
                    if (picture == NULL)
                    {
                        SDL_Log("ERROR : Loading image > %s\n",IMG_GetError());
                        exit(EXIT_FAILURE);
                    }
                    
                    if(!is_empty_case(picture))
                        image_to_mat(picture,input[i * 9 + j]);

                    else
                        sdk[i * 9 + j] = '.';

                    SDL_FreeSurface(picture);                         
  
                }
            }
            return_result(input,&net,sdk);
            print_result(sdk,"my_grid");
        }

        else
        {
            int nb_image = 57;

            double input[nb_image * 9][784]; 

            SDL_Surface *picture = NULL;

            for(int i = 0; i < nb_image; i++)
            {
                for(int j = 0 ; j < 9; j++)
                {
                    char outputFile[35];
                
                    snprintf(outputFile, 35, "testing_data/%d/image%d.png", j + 1, i + 1);
                
                    picture = IMG_Load(outputFile);
        
                    if (picture == NULL)
                    {
                        SDL_Log("ERROR : Loading image > %s\n",IMG_GetError());
                        exit(EXIT_FAILURE);
                    }

                    image_to_mat(picture,input[i * 9 + j]);
        
                    SDL_FreeSurface(picture);   
                }
        
            }

            testing(input,nb_image * 9,&net);
        }
        
    }

    
    Network_Free(&net);

    IMG_Quit();
    return EXIT_SUCCESS;
}
