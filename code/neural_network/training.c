#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "basic_functions.h"
#include "neuron.h"
#include "training.h"

// Function which train the neural network with data base "training_data" during "nb_iterations" iterations

int training(double *input[], double output[][9], int nb_image, Network* net)
{
    double res[] = {0,0,0,0,0,0,0,0,0};
    int sum = 0;
    for(int i = 0; i < nb_image; i++)
    {
        feedForward(net,input[i],784, res); 
        backpropagation(net,res, output[i]); 

        if(output[i][max_tab(res)] == 1)  
            sum++;
    }
    printf("Result %d/%d\n\n",sum,nb_image);
    return 0;
}



// Function which test the neural network with data base "testing_data"
// The neural network never train on this database so if it has go answers so it's learning

void testing(double input[][784], int len, Network* net)
{
    int sum = 0;
    for(int i = 0; i < len; i++)
    {
        double res[] = {0,0,0,0,0,0,0,0,0};
        feedForward(net,input[i],784, res);


        int result = max_tab(res);
        printf("Expected %d ==> %d   (Image %d)\n\n", (i % 9) + 1, result + 1, (i / 9) + 1);

        for(int i = 0; i < 9; i++)
        {
            printf("neuron[%d] = %f\n",i+1,res[i]);
        }

        printf("\n");

        if((i % 9) + 1 == result + 1)
            sum++;
    }
    printf("%d/%d\n", sum,len);
}