#ifndef NEURON_H
#define NEURON_H

#include <stdio.h>

typedef struct Neuron
{
  int prevSize;
  double value;
  double biais;
  double *weights;
} Neuron;

typedef struct Layer
{
  int size;
  int prevSize;
  Neuron *neurons;
} Layer;

typedef struct Network
{
  int size;
  Layer *layers;
} Network;



Neuron Neuron_Create(int prevSize);
Layer Layer_Create(int size, int prevSize);
Network Network_Create(int size_layers[], int len);


void Neuron_Print(Neuron this);
void Layer_Print(Layer this, int len);
void Network_Print(Network this, int len, int size_layer[]);


void printImage(double tab[]);


void Network_Free(Network *this);


double sigmoid(double z);
double sigmoid_prime(double sigmoid);
double sumInput(Neuron *this, Layer *prevLayer);



void propogationLayer(Layer *layer, Layer *prevLayer);
void feedForward(Network *this, double inputs[], int nb_inputs, double res[]);


double output_distance(double output_need, double result);
void backpropagation(Network *this, double output[], double output_need[]);



double random_gaussian();
double randoms_bis(int max);


#endif
