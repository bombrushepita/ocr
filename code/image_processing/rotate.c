#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdlib.h>
#include <math.h>
#include "basic_functions.h"
#include "rotate.h"


SDL_Surface* rotate(double angle, SDL_Surface *imput) 
{
    double radians = toRadians(angle);
    double cosi = cos(radians);
    double sini = sin(radians);

    int widthI = imput->w;
    int heightI = imput->h;


    int diag = sqrt(widthI*widthI+heightI*heightI);

    int widthO = diag;
    int heightO = diag;

    int centerx = (int) (widthI / 2);
    int centery = (int) (heightI / 2);

    int decX = (int) ((widthO - widthI) / 2);
    int decY = (int) ((heightO - heightI) / 2);

    SDL_Surface *cp = SDL_CreateRGBSurfaceWithFormat(0, widthO, heightO, imput->format->BitsPerPixel, imput->format->format);

    for(int x = -decX ; x < widthO - decX ; x++) 
    {
        for(int y = -decY; y < heightO - decY; y++) 
        {
            int m = x - centerx;
            int n = y - centery;
            int j = ( ( int ) ( m * cosi + n * sini ) ) + centerx;
            int k = ( ( int ) ( n * cosi - m * sini ) ) + centery;
            
            if(j >= 0 && j < widthI && k >= 0 && k < heightI)
            {
                Uint32 pixel = getPixel(imput, j, k);
                put_pixel(cp, x + decX, y + decY , pixel);
            }
        }
    }

    SDL_Surface *output = useless_image_part(cp);

    SDL_FreeSurface(cp);
    return output;
}






SDL_Surface* autoRotation(SDL_Surface *surface, size_t *acc)
{
    int angle = 0;
    size_t val = (size_t) fmin(surface->w,surface->h) * 0.5;

    int radius = fmax(surface->w,surface->h);
    int acc_len = radius * 180;

    int possibleAngle = 0;
    int count = 0;

    for (int i = 0; i < acc_len && possibleAngle != -1; ++i)
    {
        int theta = i / radius;

        if (acc[i] > val)
        {
            if(possibleAngle == theta || possibleAngle == theta +  1 || possibleAngle == theta + 2)
            {
                count++;
                if(count >= 10)
                {
                    angle = possibleAngle;
                    possibleAngle = -1;
                }
            }
            else
            {
                possibleAngle = theta;
                count = 1;
            }
            
        }
    }
    SDL_Surface *output = rotate(-angle,surface);
    return output;
}