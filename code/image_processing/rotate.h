#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#ifndef ROTATE_H
#define ROTATE_H

SDL_Surface* rotate(double angle, SDL_Surface *imput);
SDL_Surface* useless_image_part(SDL_Surface *surface);
SDL_Surface* autoRotation(SDL_Surface *surface, size_t *acc);

#endif