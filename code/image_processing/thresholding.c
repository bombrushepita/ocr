#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdlib.h>
#include "basic_functions.h"
#include "thresholding.h"


void grayscale(SDL_Surface *outputsurface)
{
  for(int i = 0 ; i < outputsurface->w ; i++)
    {
        for(int j = 0 ; j < outputsurface->h ; j++)
        {
            Uint32 pixel = getPixel(outputsurface, i, j);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, outputsurface->format, &r, &g, &b);
            Uint8 avg = 0.3 * r + 0.59 * g + 0.11 * b;
            pixel = SDL_MapRGB(outputsurface->format,avg,avg,avg);
            put_pixel(outputsurface, i, j, pixel);
        }
    }
}



void precompute(SDL_Surface *image, int *result) 
{
    int width = image->w;
    int height = image->h;
    int dst = 0;
    for (int y = 0; y < height; y++) 
    {
        for (int x = 0; x < width; x++) 
        {
            Uint32 pixel = getPixel(image, x, y);
            Uint8 r,g,b;
            SDL_GetRGB(pixel, image->format, &r, &g, &b);

            int tot = r; 
            if (x > 0) 
                tot += result[dst - 1];
            if (y > 0) 
                tot += result[dst - width];
            if (x > 0 && y > 0) 
                tot -= result[dst - width - 1];
      
            result[dst] = tot;
            dst++;
        }
    }
    return;
}


int readP(int *precomputed, int w, int h, int x, int y)
{
  if (x < 0) 
    x = 0;
  else if (x >= w) 
    x = w - 1;
  if (y < 0) 
    y = 0;
  else if (y >= h) 
    y = h - 1;
  return precomputed[x + y * w];
}


void boxBlur(SDL_Surface *surface, int boxw, int boxh, SDL_Surface *output) 
{
    int width = surface->w;
    int height = surface->h;
    int bytes = width * height;
    int *precomputed = calloc(bytes,sizeof(int));
    precompute(surface, precomputed);

    double mul = 1.0 / ((boxw * 2 + 1) * (boxh * 2 + 1));
  
    for (int y = 0; y < height; y++) 
    {
        for (int x = 0; x < width; x++) 
        {
            int tot =   readP(precomputed, width, height, x + boxw, y + boxh) +
                        readP(precomputed, width, height, x - boxw, y - boxh) -
                        readP(precomputed, width, height, x - boxw, y + boxh) -
                        readP(precomputed, width, height, x + boxw, y - boxh);
      
            int val = (int) tot * mul;
            Uint32 pixel = SDL_MapRGB(output->format,val,val,val);
            put_pixel(output, x, y, pixel);
        }
    }
    free(precomputed);
    return;
}




void thresholding(SDL_Surface *surface, SDL_Surface *output, int threshold, int blurSize)
{
    grayscale(surface);
    
    int width = surface->w;
    int height = surface->h;

    SDL_Surface *blurred = SDL_CreateRGBSurfaceWithFormat(0, width, height, surface->format->BitsPerPixel, surface->format->format);
    
    boxBlur(surface, blurSize, blurSize, blurred);

    for (int y = 0; y < height - 1; y++) 
    {
        for (int x = 0; x < width; x++) 
        {
            Uint32 pixel = getPixel(blurred, x, y);
            Uint8 r, g ,b;
            SDL_GetRGB(pixel, blurred->format, &r, &g, &b);
            
            Uint32 pixel2 = getPixel(surface, x, y + 1);
            Uint8 r2, g2 ,b2;
            SDL_GetRGB(pixel2, surface->format, &r2, &g2, &b2);


            int val = 0;
            if(r - r2 > threshold)
                val = 255;

            Uint32 pixel3 = SDL_MapRGB(output->format,val,val,val);            
            put_pixel(output,x,y + 1,pixel3);
        }
  }
  SDL_FreeSurface(blurred);
  return;
}