#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include "basic_functions.h"
#include "gridDetection.h"
#include "rotate.h"


#include <gtk/gtk.h>



size_t *gridDetection(SDL_Surface *surface)
{
    int radius = fmax(surface->w,surface->h);

    size_t *acc = calloc(radius * 180, sizeof(size_t));


    for (int y = 0; y < surface->h; ++y)
    {
        for (int x = 0; x < surface->w; ++x)
        {
            Uint32 pixel = getPixel(surface, x, y);
            Uint8 r,g,b;
            SDL_GetRGB(pixel, surface->format, &r, &g, &b);

            if  (r == 0)
              continue;

            for (int i = 0; i < 180; i++)
            {
              double theta = toRadians(i);
              int rho = x * cos(theta) + y * sin(theta);
              
              acc[rho + i * radius] += 1;
            }
        }
    }

    return acc;
}



void detectMinMax(SDL_Surface *surface, int i, int j, int *minX, int *maxX, int *minY,int *maxY)
{
    int end = 0;

    if(j == 0)
        j++;
    if(j == surface->h - 1)
        j--;

    if(i == 0)
        i++;
    if(i == surface->w - 1)
        i--;


    int yBis = j;

    for(int x = i; x >= 0 && !end; x--)
    {
        Uint32 pixel = getPixel(surface, x, yBis);
        if(pixel == 0)
        {
            pixel = getPixel(surface, x, yBis - 1);
            if(pixel == 0)
            {
                pixel = getPixel(surface, x, yBis + 1);
                if(pixel == 0)
                {
                    end = 1;
                    *minX = x;
                }
                else if(yBis < surface->h - 2)
                    yBis++;   
            }
            else if(yBis > 0)
                yBis--;
        }
    }

    if(!end)
        *minX = 0;

    end = 0;
    yBis = j;

    for(int x = i; x < surface->w && !end; x++)
    {
        Uint32 pixel = getPixel(surface, x, yBis);
        if(pixel == 0)
        {
            pixel = getPixel(surface, x, yBis - 1);
            if(pixel == 0)
            {
                pixel = getPixel(surface, x, yBis + 1);
                if(pixel == 0)
                { 
                    end = 1;
                    *maxX = x;
                }
                else if(yBis < surface->h - 2)
                    yBis++;
            }
            else if(yBis > 0)
                yBis--;
        }
    }


    if(!end)
        *maxX = surface->w - 1;




    end = 0;
    int xBis = i;
    for(int y = j; y >= 0 && !end; y--)
    {
        Uint32 pixel = getPixel(surface, xBis, y);
        if(pixel == 0)
        {
            pixel = getPixel(surface, xBis - 1, y);
            if(pixel == 0)
            {
                pixel = getPixel(surface, xBis + 1, y);
                if(pixel == 0)
                {
                    end = 1;
                    *minY = y;
                }
                else if(xBis < surface->w - 2)
                    xBis++;   
            }
            else if(xBis > 0)
                xBis--;
        }
    }

    if(!end)
        *minY = 0;

    end = 0;
    xBis = i;

    for(int y = j; y < surface->h && !end; y++)
    {
        Uint32 pixel = getPixel(surface, xBis, y);
        if(pixel == 0)
        {
            pixel = getPixel(surface, xBis - 1, y);
            if(pixel == 0)
            {
                pixel = getPixel(surface, xBis + 1, y);
                if(pixel == 0)
                {
                    end = 1;
                    *maxY = y;
                }
                else if(xBis < surface->w - 2)
                    xBis++;
            }
            else if(xBis > 0)
                xBis--;
        }
    }


    if(!end)
        *maxY = surface->h - 1;

}


void addValueTab(int tabIndex[], int tabNb[], int *len, int value)
{
    int i = 0;
    int found = 0;
    while(i < *len && !found)
    {
        int diff = value - tabIndex[i];
        if(diff < 10 && diff > -10)
        {
            found = 1;
            tabNb[i]++;
        }
        i++;
    }
    if(!found)
    {
        tabIndex[*len] = value;
        tabNb[*len] = 1;
        *len+=1;
    }
}


int maxTab(int tabIndex[], int tabNb[], int len)
{
    int max = 0;
    for(int i = 0; i < len; i++)
    {
        if(tabNb[i] > tabNb[max])
            max = i;
    }
    return tabIndex[max];
}



SDL_Surface* findGrid(SDL_Surface *surface)
{
    int w = surface->w;
    int h = surface->h;

    int *Xtab = calloc(w,sizeof(int));
    int *Ytab = calloc(h,sizeof(int));

    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < h; j++)
        {
            Uint32 pixel = getPixel(surface, i, j);
            Uint8 r,g,b;
            SDL_GetRGB(pixel, surface->format, &r, &g, &b);

            if(r != 0)
            {
                Xtab[i] += 1;
                Ytab[j] += 1;
            }
        }
    }


    int *Xline = calloc(50,sizeof(int));
    int *Yline = calloc(50,sizeof(int));

    int minX, maxX, minY, maxY;

    int lenX = 0;

    for(int i = 0; i < w; i++)
    {
        if(Xtab[i] > (int) (0.4 * h))
        {
            if(i > 0 && Xtab[i - 1] < (int) (0.4 * h))
            {
                Xline[lenX] = i;
                lenX++;
            }
        }
    }

    int lenY = 0;

    for(int j = 0; j < h; j++)
    {
        if(Ytab[j] > (int) (0.4 * w))
        {
            if(j > 0 && Ytab[j - 1] < (int) (0.4 * w))
            {
                Yline[lenY] = j;
                lenY++;
            }
        }
    }

    free(Xtab);
    free(Ytab);

    int tabIndexMinX[lenX * lenY + 1];
    int tabNbMinX[lenX * lenY + 1];
    int lenMinX = 0;

    int tabIndexMaxX[lenX * lenY + 1];
    int tabNbMaxX[lenX * lenY + 1];
    int lenMaxX = 0; 

    int tabIndexMinY[lenY * lenX + 1];
    int tabNbMinY[lenY * lenX + 1];
    int lenMinY = 0;

    int tabIndexMaxY[lenY * lenX + 1];
    int tabNbMaxY[lenY * lenX + 1]; 
    int lenMaxY = 0;


    for(int indexX = 0; indexX < lenX; indexX++)
    {
        for(int indexY = 0; indexY < lenY; indexY++)
        {
            int accMinX, accMaxX, accMinY, accMaxY;
            detectMinMax(surface,Xline[indexX],Yline[indexY],&accMinX,&accMaxX,&accMinY,&accMaxY);
            addValueTab(tabIndexMinX,tabNbMinX,&lenMinX,accMinX);
            addValueTab(tabIndexMaxX,tabNbMaxX,&lenMaxX,accMaxX);
            addValueTab(tabIndexMinY,tabNbMinY,&lenMinY,accMinY);
            addValueTab(tabIndexMaxY,tabNbMaxY,&lenMaxY,accMaxY);
        }
    }

    free(Xline);
    free(Yline);

    minX = maxTab(tabIndexMinX,tabNbMinX,lenMinX);
    maxX = maxTab(tabIndexMaxX,tabNbMaxX,lenMaxX);
    minY = maxTab(tabIndexMinY,tabNbMinY,lenMinY);
    maxY = maxTab(tabIndexMaxY,tabNbMaxY,lenMaxY);

    if(maxX <= minX || maxY <= minY || minX < 0 || minY < 0)
        return NULL;

    return rescale(surface, minX, maxX, minY, maxY);
}

