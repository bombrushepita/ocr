#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#ifndef THRESHOLD_H
#define THRESHOLD_H

void thresholding(SDL_Surface *surface, SDL_Surface *output, int threshold, int blurSize);
void grayscale(SDL_Surface *outputsurface);
void boxBlur(SDL_Surface *surface, int boxw, int boxh, SDL_Surface *output) ;
int readP(int *precomputed, int w, int h, int x, int y);
void precompute(SDL_Surface *image, int *result);


#endif