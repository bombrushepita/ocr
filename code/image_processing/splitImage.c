#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <gtk/gtk.h>
#include "basic_functions.h"
#include "splitImage.h"


// > 1000 => 25 // bin: 90,90
// > 800 => 17  // bin: 20,20
// > 500 => 11  // bin: 20,20



int widthRatio(SDL_Surface *surface);
int heightRatio(SDL_Surface *surface);
void resizePicture(SDL_Surface *input, SDL_Surface *output);


int splitImage(SDL_Surface *picture)
{
    if (picture->w < 46)
        return 1;

    double PRECISION = picture->w / 46;

    // Get width ratio and y ratio
    int ratioX = picture->w / 9;
    int ratioY = picture->h / 9;


    // Display all split surface
    int movei = 0;
    int movej = 0;

    for (int j = 0; j < 9; ++j)
    {
        for (int i = 0; i < 9; ++i)
        {
            movei = i * ratioX + PRECISION;
            movej = j * ratioY + PRECISION;

            int xCut = ratioX - 2 * PRECISION;
            int yCut = ratioY - 2 * PRECISION;

            SDL_Surface *acc = SDL_CreateRGBSurfaceWithFormat(
                0, xCut, yCut, picture->format->BitsPerPixel,
                picture->format->format);
            
            SDL_Rect rect = {movei, movej, xCut, yCut};

            if (SDL_BlitSurface(picture, &rect, acc, NULL) != 0)
            {
                SDL_Log("ERROR : Creating cut image > %s\n",IMG_GetError());
                exit(EXIT_FAILURE);
            }

            SDL_Surface *acc2 = useless_image_part(acc);

            SDL_Surface *output = SDL_CreateRGBSurfaceWithFormat(0, 28, 28, 
                picture->format->BitsPerPixel, picture->format->format);

            resizePicture(acc,output);

            if(is_empty_case(output))
            {
                char outputFile[30];
                SDL_Surface* black = IMG_Load("needed/empty.png");
                snprintf(outputFile, 30, "output/test%d%d.png", i, j);
                IMG_SavePNG(black, outputFile);
                SDL_FreeSurface(black);
            }
            else
            {
                resizePicture(acc2, output);

                char outputFile[18];
                snprintf(outputFile, 18, "output/test%d%d.png", i, j);
                IMG_SavePNG(output, outputFile);
            }

            SDL_FreeSurface(output);
            SDL_FreeSurface(acc);
            SDL_FreeSurface(acc2);
        }
    }
    return 0;
}


// Function which resize the picture for a lenght of 28 * 28

void resizePicture(SDL_Surface *input, SDL_Surface *output)
{
    double x = (double) input->w / 28;
    double y = (double) input->h / 28;

    for(int i = 0; i < 28; i++)
    {
        for(int j = 0; j < 28; j++)
        {
            Uint32 pixel = getPixel(input,i * x,j * y);
            put_pixel(output,i,j,pixel);
        }
    }
}
