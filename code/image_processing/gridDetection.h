#ifndef GRID_DETECTION_H
#define GRID_DETECTION_H

size_t *gridDetection(SDL_Surface *surface);
SDL_Surface* findGrid(SDL_Surface *surface);

#endif