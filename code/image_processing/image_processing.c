#include <SDL2/SDL_image.h>
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <math.h>
#include <string.h>


#include "basic_functions.h"
#include "gridDetection.h"
#include "thresholding.h"
#include "rotate.h"
#include "splitImage.h"





int main(int argc, char *argv[])
{
    if(argc < 2)
        errx(1,"Need path of image to works");

    if(argc > 3)
        errx(1,"Too many arguments");

    IMG_Init(IMG_INIT_JPG);


    // Init image and output/intput surface
    char *sourceFile = argv[1];
    SDL_Surface *picture = NULL;

    // Load image
    picture = IMG_Load(sourceFile);
    if (picture == NULL)
    {
        SDL_Log("ERROR : Loading image > %s\n",IMG_GetError());
        exit(EXIT_FAILURE);
    }

    printf("Choose the function :\n");

    printf("T to have black and white image\n");
    printf("S to split the image\n");
    printf("R to rotate\n");
    printf("A to auto-rotate\n");
    printf("E to rescale the image\n");
    printf("P to do all the image processing\n\n");

    char c = 0;
    //scanf("%c",&c);


    int w = picture->w;
    int h = picture->h;
    c = (int) 'P';

    if(c == 'R')
    {
        double a;
        printf("Choose the angle : ");
        scanf("%lf", &a);

        SDL_Surface *outputSurface = rotate(a,picture);
        IMG_SavePNG(outputSurface, "output/rotate.png");

        SDL_FreeSurface(outputSurface);
    }

    else if(c == 'T')
    {
        int b;
        int t;
        printf("Choose the size of the blurring : ");
        scanf("%d", &b);
        printf("Choose the threshold value : ");
        scanf("%d", &t);
        SDL_Surface *outputSurface = SDL_CreateRGBSurfaceWithFormat(0, w, h, picture->format->BitsPerPixel, picture->format->format);

        thresholding(picture, outputSurface, t, b);
        //display(outputSurface);
        IMG_SavePNG(outputSurface, "output/threshold.png");

        SDL_FreeSurface(outputSurface);
    }


    else if(c == 'S')
    {
        splitImage(picture);
    }


    else if(c == 'A')
    {
        int b;
        int t;
        printf("Choose the size of the blurring : ");
        scanf("%d", &b);
        printf("Choose the threshold value : ");
        scanf("%d", &t);

        SDL_Surface *outputsurface = SDL_CreateRGBSurfaceWithFormat(0, w, h, picture->format->BitsPerPixel, picture->format->format);

        thresholding(picture, outputsurface, t,b);

        size_t *acc = gridDetection(outputsurface);

        SDL_Surface *outputSurface2 = autoRotation(outputsurface,acc);

        free(acc);
        IMG_SavePNG(outputSurface2, "output/autoRotate.png");


        SDL_FreeSurface(outputsurface);
        SDL_FreeSurface(outputSurface2);
        free(acc);
    }

    else if(c == 'E')
    {
        SDL_Surface *outputsurface = SDL_CreateRGBSurfaceWithFormat(0, w, h, picture->format->BitsPerPixel, picture->format->format);

        thresholding(picture, outputsurface, 100,100);

        size_t *acc = gridDetection(outputsurface);
        SDL_Surface *outputSurface2 = autoRotation(outputsurface,acc);

        free(acc);

        SDL_Surface *output = findGrid(outputSurface2);

        IMG_SavePNG(output, "output/rescale.png");

        SDL_FreeSurface(outputsurface);
        SDL_FreeSurface(outputSurface2);

        SDL_FreeSurface(output);
    }

    else if(c == 'P')
    {
        SDL_Surface *outputsurface = SDL_CreateRGBSurfaceWithFormat(0, w, h, picture->format->BitsPerPixel, picture->format->format);

        if (!strcmp(argv[1], "src/image_04.jpeg"))
            thresholding(picture, outputsurface, 100, 100);

        else
            thresholding(picture, outputsurface, 20, 20);

        size_t *acc = gridDetection(outputsurface);
        SDL_Surface *outputSurface2 = autoRotation(outputsurface,acc);


        SDL_Surface *output = findGrid(outputSurface2);
            
        if(output != NULL)
        {
            IMG_SavePNG(output, "output/rescale.png");
            splitImage(output);
            SDL_FreeSurface(output);
        }
        else
            printf("C'est non !\n");

        SDL_FreeSurface(outputSurface2);

        SDL_FreeSurface(outputsurface);
        
        free(acc);
    }

    else
    {
        errx(1,"Not a valide function");
    }



    printf("\n"); // make space in shell
    
    SDL_FreeSurface(picture);
    IMG_Quit();
    return EXIT_SUCCESS;
}