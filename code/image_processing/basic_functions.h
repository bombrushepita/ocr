#ifndef BASIC_FUNCTIONS_H
#define BASIC_FUNCTIONS_H

void display(SDL_Surface *surface);
Uint32 getPixel(SDL_Surface *surface, int x, int y);
void cleanResources(SDL_Window *w, SDL_Renderer *r, SDL_Texture *t);
void put_pixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
SDL_Surface* rescale(SDL_Surface *surface, int minX, int maxX, int minY,int maxY);
double toRadians(double angle);
SDL_Surface* useless_image_part(SDL_Surface *surface);
int is_empty_case(SDL_Surface *surface);

#endif