#ifndef BASIC_FUNCTIONS_H
#define BASIC_FUNCTIONS_H

Uint32 getPixel(SDL_Surface *surface, int x, int y);
void put_pixel(SDL_Surface *surface, int x, int y, Uint32 pixel);

double abso(double c);
int max_tab(double tab[]);

void image_to_mat(SDL_Surface *surface, double *tab);
int is_empty_case(SDL_Surface *surface);

SDL_Surface* rescale(SDL_Surface *surface, int minX, int maxX, int minY,int maxY);
double toRadians(double angle);
SDL_Surface* useless_image_part(SDL_Surface *surface);

#endif