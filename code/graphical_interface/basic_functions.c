#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdlib.h>
#include <stdio.h>
#include "basic_functions.h"

#include <gtk/gtk.h>



Uint32 getPixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;

    // Here p is the address to the pixel we want to retrieve 
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch (bpp)
    {
        case 1:
            return *p;
            break;

        case 2:
            return *(Uint16 *)p;
            break;

        case 3:
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;
            break;

        case 4:
            return *(Uint32 *)p;
            break;

        default:
            return 0;
    }
}



void put_pixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    Uint8  bpp = surface->format->BytesPerPixel;
    Uint8 *p   = (Uint8 *) surface->pixels + y * surface->pitch + x * bpp;

    switch (bpp)
    {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint16 *) p = pixel;
        break;

    case 3:
        if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
        {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        }
        else
        {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }

        break;

    case 4:
        *(Uint32 *) p = pixel;
        break;

    default:
        break;
    }
}




SDL_Surface* rescale(SDL_Surface *surface, int minX, int maxX, int minY,int maxY)
{
    SDL_Surface *cp = SDL_CreateRGBSurfaceWithFormat(0, maxX - minX, maxY - minY, surface->format->BitsPerPixel, surface->format->format);
    for(int j = minY; j < maxY; j++)
    {
        for(int i = minX; i < maxX; i++)
        {
            Uint32 pixel = getPixel(surface, i, j);
            put_pixel(cp, i - minX, j - minY, pixel);
        }
    }
    return cp;
}



double toRadians(double angle)
{
    double PI = 3.14159265358979323846264338;
    return angle * (PI / 180);
}




SDL_Surface* useless_image_part(SDL_Surface *surface)
{
    int* tuple = calloc(4, sizeof(int));

    int finish = 0;

    for(int i = 0; i < surface->w && !finish; i++)
    {
        for(int j = 0; j < surface->h && !finish; j++)
        {
            Uint32 pixel = getPixel(surface, i, j);
            Uint8 r,g,b;
            SDL_GetRGB(pixel,surface->format,&r,&g,&b);
            if(r > 100)
            {
                tuple[0] = i;
                finish = 1;
            }
        }
    }

    finish = 0;

    for(int j = 0; j < surface->h && !finish; j++)
    {
        for(int i = 0; i < surface->w && !finish; i++)
        {
            Uint32 pixel = getPixel(surface, i, j);
            Uint8 r,g,b;
            SDL_GetRGB(pixel,surface->format,&r,&g,&b);
            if(r > 100)
            {
                tuple[1] = j;
                finish = 1;
            }
        }
    }



    finish = 0;

    for(int i = surface->w - 1; i >= 0 && !finish; i--)
    {
        for(int j = surface->h - 1; j >= 0  && !finish; j--)
        {
            Uint32 pixel = getPixel(surface, i, j);
            Uint8 r,g,b;
            SDL_GetRGB(pixel,surface->format,&r,&g,&b);
            if(r > 100)
            {
                tuple[2] = i;
                finish = 1;
            }
        }
    }

    finish = 0;

    for(int j = surface->h - 1; j >= 0  && !finish; j--)
    {
        for(int i = surface->w - 1; i >= 0 && !finish; i--)
        {
            Uint32 pixel = getPixel(surface, i, j);
            Uint8 r,g,b;
            SDL_GetRGB(pixel,surface->format,&r,&g,&b);
            if(r > 100)
            {
                tuple[3] = j;
                finish = 1;
            }
        }
    }

    SDL_Surface *output;

    if(tuple[0] >= tuple[2] || tuple[1] >= tuple[3])
        output = rescale(surface,0,surface->w - 1, 0,surface->h - 1);
    else
        output = rescale(surface,tuple[0],tuple[2], tuple[1],tuple[3]);

    free(tuple);
    return output;
}


// Function of absolute value

double abso(double c)
{
    if(c < 0)
        return -c;
    return c;
}


// Function which find the index of the max value in the tab of lenght 9

int max_tab(double tab[])
{
    int index = 0;
    for(int i = 1; i < 9; i++)
    {
        if(tab[i] > tab[index])
            index = i;
    }
    return index;
}



// Function which complete a board of 0 and 1 depending of the pixel of the image

void image_to_mat(SDL_Surface *surface, double *tab)
{
    for(int i = 0; i < 28; i++)
    {
        for(int j = 0; j < 28; j++)
        {
            Uint32 pixel = getPixel(surface, i, j);
            Uint8 r, g ,b;
            SDL_GetRGB(pixel, surface->format, &r, &g, &b);
            if(r > 127)
                tab[i * 28 + j] = 1.0;
            else
                tab[i * 28 + j] = 0.0;
        }
    }
}



// Function which checked if the image is considered like an empty case or not

int is_empty_case(SDL_Surface *surface)
{
    int sum = 0;
    for(int i = 0; i < 28; i++)
    {
        for(int j = 0; j < 28; j++)
        {
            Uint32 pixel = getPixel(surface,i,j);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, surface->format, &r, &g, &b);

            if(r != 0)
                sum++;
        }
    }

    if(sum < 20)
        return 1;
    return 0;
}