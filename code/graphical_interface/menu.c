#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <gtk/gtk.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <err.h>
#include <time.h>

#include "../graphical_interface/basic_functions.h"

#include "../image_processing/thresholding.h"
#include "../image_processing/rotate.h"
#include "../image_processing/gridDetection.h"
#include "../image_processing/splitImage.h"

#include "../neural_network/neuron.h"
#include "../neural_network/save_weights.h"
#include "../neural_network/training.h"
#include "../neural_network/find_result.h"


#include "../solver/solve.h"
#include "../solver/return_image.h"

#define SIZE 600


typedef enum State
{
    CHOOSE,
    BINARIZE,
    ROTATION,
    RESCALE,
    SOLVE,

} State;


typedef struct Binarize
{
    guint threshold_scale;
    guint blur_scale;

} Binarize;



typedef struct Rotation
{
    guint angle;
    gboolean autorotation;

} Rotation;


// Structure of all SDL_Surface
typedef struct Image
{
    SDL_Surface* picture;

    SDL_Surface* binarization;
    SDL_Surface* rotation;
    SDL_Surface* rescale;

} Image;


typedef struct Training
{
    guint nb_training;

} Training;


// Structure of the graphical user interface.
typedef struct UserInterface
{
    GtkWindow *window;

    GtkButton *binarization;
    GtkScale *blur;
    GtkScale *threshold;

    GtkButton *rotation;
    GtkCheckButton *autorotation;
    GtkScale *angle;

    GtkButton *rescale;

    GtkFileChooserButton* load;
    GtkImage* image;
    GtkButton* return_button;

    GtkButton* solver;

    GtkScale* training_scale;
    GtkButton* training;

} UserInterface;


typedef struct Menu
{
    State state;
    Image image;
    Binarize binarize;
    Rotation rotation;
    Training training;
    UserInterface ui;

} Menu;



void gtk_free_surface(Menu *menu)
{
    if(menu->image.picture != NULL)
    {
        SDL_FreeSurface(menu->image.picture);
        menu->image.picture = NULL;
    }

    if(menu->image.binarization != NULL)
    {
        SDL_FreeSurface(menu->image.binarization);
        menu->image.binarization = NULL;
    }

    if(menu->image.rotation != NULL)
    {
        SDL_FreeSurface(menu->image.rotation);
         menu->image.rotation = NULL;
    }

    if(menu->image.rescale != NULL)
    {
        SDL_FreeSurface(menu->image.rescale);
        menu->image.rescale = NULL;
    }
}


void nosolution(Menu *menu)
{
    GtkWidget *dialog2 = gtk_message_dialog_new_with_markup(menu->ui.window,
            GTK_DIALOG_USE_HEADER_BAR,
            GTK_MESSAGE_INFO,
            GTK_BUTTONS_CLOSE,
            "There isn't a solution");

    gtk_message_dialog_format_secondary_markup(GTK_MESSAGE_DIALOG(dialog2),"<b>This Sudoku does not have a solution</b>");

    gtk_dialog_run(GTK_DIALOG(dialog2));

    gtk_widget_destroy(dialog2);
}

void load_image(GtkFileChooserButton *file_chooser, gpointer data)
{
    Menu* menu = data;

    if(menu->state == CHOOSE)
    {

        GtkWidget *preview;
        char *filename;
        GdkPixbuf *pixbuf;

        preview = GTK_WIDGET(menu->ui.image);
        filename = gtk_file_chooser_get_preview_filename(GTK_FILE_CHOOSER(file_chooser));

        if(filename != NULL)
        {
            pixbuf = gdk_pixbuf_new_from_file_at_size(filename, SIZE, SIZE, NULL);

            gtk_image_set_from_pixbuf(GTK_IMAGE(preview), pixbuf);
            if(pixbuf)
                g_object_unref(pixbuf);
        }


        // Load image

        SDL_Surface* picture = NULL;

        picture = IMG_Load(filename);

        if(picture != NULL)
        {
            IMG_SavePNG(picture, "output/picture.png");
            menu->image.picture = picture;
            menu->state = BINARIZE;
            gtk_widget_set_sensitive(GTK_WIDGET(file_chooser),FALSE);
            gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.binarization), TRUE);
            gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.return_button), TRUE);
        }
        g_free(filename);
    }
}


void recharge_image(char *filename, gpointer user_data)
{
    Menu* menu = user_data;

    GtkWidget *preview = GTK_WIDGET(menu->ui.image);

    GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file_at_size(filename, SIZE, SIZE, NULL);

    gtk_image_set_from_pixbuf(GTK_IMAGE(preview), pixbuf);
    if(pixbuf)
        g_object_unref(pixbuf);
}


void blur_change(GtkRange* range, gpointer user_data)
{
    Menu *menu = user_data;
    menu->binarize.blur_scale = gtk_range_get_value(range);
}


void threshold_change(GtkRange* range, gpointer user_data)
{
    Menu *menu = user_data;
    menu->binarize.threshold_scale = gtk_range_get_value(range);
}


void binarize(GtkButton *button, gpointer user_data)
{
    Menu *menu = user_data;
    if(menu->state == BINARIZE)
    {
        int t = menu->binarize.threshold_scale;
        int b = menu->binarize.blur_scale;

        SDL_Surface *picture = menu->image.picture;

        int w = picture->w;
        int h = picture->h;

        SDL_Surface *threshold = SDL_CreateRGBSurfaceWithFormat(0, w, h, picture->format->BitsPerPixel, picture->format->format);

        thresholding(picture, threshold, t, b);

        if(threshold == NULL)
        {
            gtk_free_surface(menu);
            errx(1,"Probleme of thresholding image");
        }

        menu->image.binarization = threshold;
        menu->state = ROTATION;

        IMG_SavePNG(menu->image.binarization, "output/binarize.png");
        recharge_image("output/binarize.png", menu);

        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.binarization),FALSE);
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.rotation), TRUE);
    }

}

void auto_choose(GtkToggleButton* button, gpointer user_data)
{
    Menu *menu = user_data;
    menu->rotation.autorotation = gtk_toggle_button_get_active(button);
}


void angle_change(GtkRange* range, gpointer user_data)
{
    Menu *menu = user_data;
    menu->rotation.angle = gtk_range_get_value(range);
}


void gtk_rotate(GtkButton *button, gpointer user_data)
{
    Menu *menu = user_data;
    if(menu->state == ROTATION)
    {

        SDL_Surface *picture = menu->image.binarization;

        if(menu->rotation.autorotation)
        {
            size_t *acc = gridDetection(menu->image.binarization);
            menu->image.rotation = autoRotation(menu->image.binarization,acc);
            free(acc);
        }
        else
        {
            int a = menu->rotation.angle;
            menu->image.rotation = rotate(a,picture);
        }

        if(menu->image.rotation == NULL)
        {
            gtk_free_surface(menu);
            errx(1,"Probleme of rotatating image");
        }

        menu->state = RESCALE;
        IMG_SavePNG(menu->image.rotation, "output/rotate.png");
        recharge_image("output/rotate.png", menu);

        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.rotation),FALSE);
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.rescale), TRUE);
    }
}



void gtk_rescale(GtkButton *button, gpointer user_data)
{
    Menu *menu = user_data;
    if(menu->state == RESCALE)
    {

        SDL_Surface *picture = menu->image.rotation;

        menu->image.rescale = findGrid(picture);

        if(menu->image.rescale == NULL)
        {
            gtk_free_surface(menu);
            errx(1,"Probleme of rescaling image");
        }

        menu->state = SOLVE;
        IMG_SavePNG(menu->image.rescale, "output/rescale.png");
        recharge_image("output/rescale.png", menu);

        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.rescale),FALSE);
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.solver), TRUE);
    }
}




void gtk_network(Network *net, int sdk[81])
{
    double input[81][784];

    SDL_Surface *split = NULL;

    for(int i = 0; i < 9; i++)
    {
        for(int j = 0; j < 9; j++)
        {
            char outputFile[35];

            snprintf(outputFile, 35, "output/test%d%d.png", i, j);

            split = IMG_Load(outputFile);

            if(split == NULL)
            {
                SDL_Log("ERROR : Loading image > %s\n",IMG_GetError());
                exit(EXIT_FAILURE);
            }

            if(!is_empty_case(split))
                image_to_mat(split,input[j * 9 + i]);

            else
                sdk[j * 9 + i] = -1;

            SDL_FreeSurface(split);

        }
    }
    return_result(input,net,sdk);

    for(int i = 0; i < 81; i++)
    {
        if(sdk[i] == -1)
            sdk[i] = 0;
    }
}



void gtk_solver(GtkButton *button, gpointer user_data)
{
    Menu *menu = user_data;
    if(menu->state == SOLVE)
    {
        if(splitImage(menu->image.rescale))
        {
            nosolution(menu);
            return;
        }

        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.solver),FALSE);

        int network_size[] = {784,30,9};
        Network net;
        net = Recovery("weights_biais",network_size);

        int sdk[81];

        for(int i = 0; i < 81; i++)
            sdk[i] = 0;

        gtk_network(&net,sdk);

        Network_Free(&net);

        save_result(sdk,"output/my_grid");

        if(solve(sdk))
        {
            nosolution(menu);
            return;
        }
        else
            save_result(sdk,"output/my_grid");

    }

    return_image("output/my_grid");
    recharge_image("output/result.png", user_data);

    gtk_free_surface(menu);

    gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.return_button),FALSE);
    gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.load),TRUE);
    menu->state = CHOOSE;

}




void gtk_return_button(GtkButton *button, gpointer user_data)
{
    Menu *menu = user_data;
    if(menu->state == SOLVE)
    {
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.rescale),TRUE);
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.solver),FALSE);
        menu->state = RESCALE;
        recharge_image("output/rotate.png", menu);
        SDL_FreeSurface(menu->image.rescale);
    }
    else if(menu->state == RESCALE)
    {
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.rotation),TRUE);
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.rescale),FALSE);
        menu->state = ROTATION;
        recharge_image("output/binarize.png", menu);
        SDL_FreeSurface(menu->image.rotation);
    }
    else if(menu->state == ROTATION)
    {
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.binarization),TRUE);
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.rotation),FALSE);
        menu->state = BINARIZE;
        recharge_image("output/picture.png", menu);
        SDL_FreeSurface(menu->image.binarization);
    }
    else if(menu->state == BINARIZE)
    {
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.load),TRUE);
        gtk_widget_set_sensitive(GTK_WIDGET(menu->ui.binarization),FALSE);
        menu->state = CHOOSE;
        gtk_image_clear(menu->ui.image);
        SDL_FreeSurface(menu->image.picture);
        gtk_widget_set_sensitive(GTK_WIDGET(button),FALSE);
    }
}



void nb_traning_change(GtkRange* range, gpointer user_data)
{
    Menu *menu = user_data;
    menu->training.nb_training = gtk_range_get_value(range);
}





void gtk_training(GtkButton *button, gpointer user_data)
{
    Menu *menu = user_data;
    GtkWidget *dialog = gtk_message_dialog_new_with_markup(menu->ui.window,
            GTK_DIALOG_USE_HEADER_BAR,
            GTK_MESSAGE_INFO,
            GTK_BUTTONS_NONE,
            "\nWaiting for training");

    gtk_dialog_add_button(GTK_DIALOG(dialog),"Start", GTK_RESPONSE_OK);


    gtk_dialog_run(GTK_DIALOG(dialog));

    gtk_widget_destroy(dialog);


    int network_size[] = {784,30,9};
    Network net;
    net = Recovery("weights_biais",network_size);


    int nb_iterations = menu->training.nb_training;

    int nb_image = 515;


    for(int g = 0; g < nb_iterations; g++)
    {
        double *input[9];

        for(int i = 0; i < 9; i++)
            input[i] = calloc(784,sizeof(double));

        double output[9][9];

        SDL_Surface *picture = NULL;

        int i = randoms_bis(nb_image);

        for(int j = 0; j < 9; j++)
        {
            char outputFile[35];

            snprintf(outputFile, 35, "training_data/%d/image%d.png", j + 1 , i + 1);

            picture = IMG_Load(outputFile);

            if(picture == NULL)
            {
                SDL_Log("ERROR : Loading image > %s\n",IMG_GetError());
                exit(EXIT_FAILURE);
            }


            for(int k = 0; k < 9; k++)
                output[j][k] = 0;
            output[j][j] = 1;


            image_to_mat(picture,input[j]);

            SDL_FreeSurface(picture);
        }

        training(input, output, 9, &net);

        for(int k = 0; k < 9; k++)
            free(input[k]);
    }


    GtkWidget *dialog2 = gtk_message_dialog_new_with_markup(menu->ui.window,
            GTK_DIALOG_USE_HEADER_BAR,
            GTK_MESSAGE_INFO,
            GTK_BUTTONS_YES_NO,
            "The training is terminated");

    gtk_message_dialog_format_secondary_markup(GTK_MESSAGE_DIALOG(dialog2),"<b>Do you want to save it ?</b>");

    gint result = gtk_dialog_run(GTK_DIALOG(dialog2));

    switch (result)
    {
        case GTK_RESPONSE_YES:
            SaveNetworkData(&net,network_size,"weights_biais");
            break;
        default:
            break;
    }
    Network_Free(&net);

    gtk_widget_destroy(dialog2);
}

void gtk_leave_button(GtkButton* button, gpointer user_data)
{
    Menu *menu = user_data;

    gtk_free_surface(menu);

    gtk_widget_destroy(GTK_WIDGET(menu->ui.window));
}

int main (int argc, char *argv[])
{
    // Initializes GTK.
    gtk_init(NULL, NULL);

    // Constructs a GtkBuilder instance.
    GtkBuilder* builder = gtk_builder_new();

    // Loads the UI description.
    // (Exits if an error occurs.)
    GError* error = NULL;
    if (gtk_builder_add_from_file(builder, "needed/menu2.glade", &error) == 0)
    {
        g_printerr("Error loading file: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    }


    //Gets the widgets
    GtkWindow *window = GTK_WINDOW(gtk_builder_get_object(builder, "menu"));


    GtkButton *binarization = GTK_BUTTON(gtk_builder_get_object(builder, "rotation1"));
    GtkScale* blur = GTK_SCALE(gtk_builder_get_object(builder, "slider binarisation"));
    GtkScale* threshold = GTK_SCALE(gtk_builder_get_object(builder, "slider binarisation1"));


    GtkButton *rotation = GTK_BUTTON(gtk_builder_get_object(builder, "rotation"));
    GtkCheckButton *autorotation = GTK_CHECK_BUTTON(gtk_builder_get_object(builder, "AutoRotation"));
    GtkScale* angle = GTK_SCALE(gtk_builder_get_object(builder, "slider rotation"));


    GtkButton *rescale = GTK_BUTTON(gtk_builder_get_object(builder, "resize"));


    GtkFileChooserButton* load = GTK_FILE_CHOOSER_BUTTON(gtk_builder_get_object(builder, "Load1"));
    GtkImage* image = GTK_IMAGE(gtk_builder_get_object(builder, "image"));
    GtkButton *return_button = GTK_BUTTON(gtk_builder_get_object(builder, "Return"));
    GtkButton *leave_button = GTK_BUTTON(gtk_builder_get_object(builder, "Leave"));


    GtkButton* solver = GTK_BUTTON(gtk_builder_get_object(builder, "solver"));


    GtkScale *training_scale = GTK_SCALE(gtk_builder_get_object(builder, "slider rotation1"));
    GtkButton *training = GTK_BUTTON(gtk_builder_get_object(builder, "IAbutton"));


    for(int i = 100; i >= 20;i--)
    {
        GtkAdjustment *adjustement = gtk_adjustment_new(i,20,100,10,0,0);
        gtk_range_set_adjustment(GTK_RANGE(blur),adjustement);
    }


    for(int i = 100; i >= 20;i--)
    {
        GtkAdjustment *adjustement = gtk_adjustment_new(i,20,100,10,0,0);
        gtk_range_set_adjustment(GTK_RANGE(threshold),adjustement);
    }


    for(int i = -90; i <= 90;i++)
    {
        GtkAdjustment *adjustement = gtk_adjustment_new(i,-90,90,10,0,0);
        gtk_range_set_adjustment(GTK_RANGE(angle),adjustement);
    }
    GtkAdjustment *adjustement = gtk_adjustment_new(0,-90,90,10,0,0);
    gtk_range_set_adjustment(GTK_RANGE(angle),adjustement);


    for(int i = 1000; i >= 0;i--)
    {
        GtkAdjustment *adjustement = gtk_adjustment_new(i,0,1000,10,0,0);
        gtk_range_set_adjustment(GTK_RANGE(training_scale),adjustement);
    }


    Menu menu =
    {
        .state = CHOOSE,

        .image =
        {
            .picture = NULL,
            .binarization = NULL,
            .rotation = NULL,
            .rescale = NULL,
        },

        .rotation =
        {
            .angle = 0,
            .autorotation = TRUE,
        },

        .binarize =
        {
            .threshold_scale = 20,
            .blur_scale = 20,
        },

        .training =
        {
            .nb_training = 0,
        },

        .ui =
        {
            .window = window,

            .binarization = binarization,
            .blur = blur,
            .threshold = threshold,

            .rotation = rotation,
            .autorotation = autorotation,
            .angle = angle,

            .load = load,
            .image = image,
            .return_button = return_button,

            .rescale = rescale,

            .solver = solver,

            .training_scale = training_scale,
            .training = training,
        },
    };


    // Connects event handlers.
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(load, "file-set",G_CALLBACK(load_image), &menu);


    g_signal_connect(binarization, "clicked", G_CALLBACK(binarize), &menu);
    g_signal_connect(GTK_RANGE(blur), "value-changed", G_CALLBACK(blur_change), &menu);
    g_signal_connect(GTK_RANGE(threshold), "value-changed", G_CALLBACK(threshold_change), &menu);


    g_signal_connect(rotation, "clicked", G_CALLBACK(gtk_rotate), &menu);
    g_signal_connect(GTK_TOGGLE_BUTTON(autorotation),"toggled", G_CALLBACK(auto_choose), &menu);
    g_signal_connect(GTK_RANGE(angle), "value-changed", G_CALLBACK(angle_change), &menu);


    g_signal_connect(rescale, "clicked", G_CALLBACK(gtk_rescale), &menu);


    g_signal_connect(solver, "clicked", G_CALLBACK(gtk_solver), &menu);

    g_signal_connect(return_button, "clicked", G_CALLBACK(gtk_return_button), &menu);
    g_signal_connect(leave_button, "clicked", G_CALLBACK(gtk_leave_button), &menu);


    g_signal_connect(GTK_RANGE(training_scale), "value-changed", G_CALLBACK(nb_traning_change), &menu);
    g_signal_connect(training, "clicked", G_CALLBACK(gtk_training), &menu);


    // Runs the main loop.
    gtk_main();

    // Exits.
    return 0;
}
